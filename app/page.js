"use client";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { Field, Form, Formik } from "formik";
import Link from "next/link";
import { useState } from "react";
import * as Yup from "yup";
import { usePathname } from "next/navigation";
import { loginService } from "./redux/services/admin.service";
import LoadingComponent from "./component/loading/LoadingComponent";
import { useRouter } from "next/navigation";
import ProtectedRouteLog from "./component/protected/ProtectedRouteLog";

const loginSchema = Yup.object().shape({
  username: Yup.string().required("Username can't empty"),
  // .matches(
  //   /^[A-Za-z0-9]{4,}$/,
  //   "Username must be at least 4 characters or numbers"
  // ),
  password: Yup.string().required("Password can't empty"),
});
export default function Page() {
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const [user, setUser] = useState({});
  const handleInput = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };
  const pathname = usePathname();
  const router = useRouter();

  return (
    <ProtectedRouteLog>
      <Formik
        initialValues={{
          username: "",
          password: "",
        }}
        validationSchema={loginSchema}
        onSubmit={(values, { setFieldError }) => {
          setLoading(true);
          console.log(pathname);
          loginService(values).then((result) => {
            if (result.status === 200) {
              console.log(result.data.token, "login successful");
              router.push("/dashboard");

              if (pathname === "/dashboard") {
                setLoading(false);
              }
            } else {
              // Set the 'username' field error using Formik's setFieldError
              setFieldError("username", "Invalid username or password");
              setLoading(false);
            }
          });
        }}
      >
        {({ errors, touched }) => (
          <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden">
            <div className="w-full p-6 py-16 bg-white rounded-md shadow-md lg:max-w-xl">
              <h1 className="text-3xl font-bold text-center text-gray-700">
                Digital Asset
              </h1>
              <Form className="mt-6" onChange={handleInput}>
                <div className="mb-4">
                  <label
                    htmlFor="text"
                    className="block text-sm font-semibold text-gray-800"
                  >
                    Username
                  </label>
                  <Field
                    type="text"
                    placeholder="example70"
                    name="username"
                    className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                  />
                  {errors.username && touched.username ? (
                    <div className="text-red-500 text-[14.4px] mt-2">
                      {errors.username}
                    </div>
                  ) : null}
                </div>
                <div className="mb-2 relative">
                  <label
                    htmlFor="password"
                    className="block text-sm font-semibold text-gray-800"
                  >
                    Password
                  </label>
                  <Field
                    type={showPassword ? "text" : "password"}
                    placeholder="Password"
                    name="password"
                    // type="password"
                    className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                  />
                  {user.password && (
                    <FontAwesomeIcon
                      icon={showPassword ? faEye : faEyeSlash}
                      onClick={togglePasswordVisibility}
                      className="absolute inset-y-0 top-10 right-0 flex items-center pr-3 text-iconcolor cursor-pointer"
                    />
                  )}
                  {errors.password && touched.password ? (
                    <div className="text-red-500 text-[14.4px]">
                      {errors.password}
                    </div>
                  ) : null}
                </div>
                {/* <Link
                href="/forget"
                className="text-xs text-blue-600 hover:underline"
              >
                Forget Password?
              </Link> */}
                <div className="mt-8">
                  <button
                    type="submit"
                    className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600"
                  >
                    {loading ? <LoadingComponent /> : "Login"}
                  </button>
                </div>
              </Form>
            </div>
          </div>
        )}
      </Formik>
    </ProtectedRouteLog>
  );
}
