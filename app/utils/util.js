import axios from "axios";
export const BASE_URL = `http://178.128.52.39:8080/api/v1`;
export const BASE_URL1 = `http://178.128.52.39:8080`;
export const BASE_URL2 = `http://8.219.142.113:3000`;
export const BASE_URL_IMAGE = `http://178.128.52.39:8080/api/v1/images?fileName=`;

let TOKEN;

// Check if running on the client-side (browser)
if (typeof window !== 'undefined') {
  TOKEN = localStorage.getItem('Token');
}

const API = axios.create({
  baseURL: BASE_URL,
  headers: {
    Authorization: `Bearer ${TOKEN}`,
    "Content-type": "application/json;charset=utf-8",
  },
});

API.interceptors.request.use((s)=> {
  s.headers.Authorization= "Bearer "+ TOKEN
  return s
})

export {API};

export const Auth = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-type": "application/json;charset=utf-8",
  },
});



export const APIImage = axios.create({
  baseURL: BASE_URL,
  headers: {
    Authorization: `Bearer ${TOKEN}`,
    "Content-Type": "multipart/form-data",
  },
});
