import { Auth } from "../../utils/util";

// login
export const loginService = async (user) => {
  try {
    const response = await Auth.post(`/auth/login`, user);
    localStorage.setItem("Token", response.data.token);
    return response;
  } catch (error) {
    return error;
  }
};