"use client";
import React, { useEffect } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Input,
  Button,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Chip,
  User,
  Pagination,
  Image,
  Divider,
} from "@nextui-org/react";
import { PlusIcon } from "./PlusIcon";
import { useState } from "react";
import { VerticalDotsIcon } from "./VerticalDotsIcon";
import { SearchIcon } from "./SearchIcon";
import { ChevronDownIcon } from "./ChevronDownIcon";
import { columns, statusOptions } from "./data";
import { capitalize } from "./utils";
import {
  ApproveUser,
  DeleteReqUser,
  GetAllUser,
  GetApproveUsers,
  GetUnApproveUsers,
  GetUserById,
} from "../../redux/services/user.service";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import { BASE_URL_IMAGE } from "../../utils/util";
import LoadingComponentGray from "../loading/LoadingComponentGray";
const statusColorMap = {
  true: "success",
  false: "warning",
};

const isMobile = () => {
  if (typeof window !== "undefined") {
    return window.innerWidth <= 768;
  }
  return false;
};

const INITIAL_VISIBLE_COLUMNS = isMobile()
  ? ["info", "actions"]
  : [
    "firstname",
    "lastname",
    "username",
    "email",
    "phoneNumber",
    "point",
    "approve",
    "actions",
  ];
console.log(INITIAL_VISIBLE_COLUMNS);

export default function TableUser1() {
  const [showModules, setShowModules] = useState(false);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [filterValue, setFilterValue] = React.useState("");
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([false]));
  const [selectedColor, setSelectedColor] = React.useState("default");
  const [visibleColumns, setVisibleColumns] = React.useState(
    new Set(INITIAL_VISIBLE_COLUMNS)
  );
  const [statusFilter, setStatusFilter] = React.useState("all");
  const [allUser, setAllUser] = React.useState([]);
  useEffect(() => {
    GetAllUser().then((res) => {
      if (res?.data?.payload?.length > 0) {
        setAllUser(res?.data?.payload);
      }
      // console.log(res);
    });
  }, []);
  console.log(statusOptions, "status");
  console.log("allUser", allUser);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [sortDescriptor, setSortDescriptor] = React.useState({
    column: "age",
    direction: "ascending",
  });
  const [page, setPage] = React.useState(1);

  const hasSearchFilter = Boolean(filterValue);

  const headerColumns = React.useMemo(() => {
    if (visibleColumns === "all") return columns;

    return columns.filter((column) =>
      Array.from(visibleColumns).includes(column.uid)
    );
  }, [visibleColumns]);

  const filteredItems = React.useMemo(() => {
    let filteredUsers = [...allUser];
    console.log(filteredUsers, "statusFilter");

    if (hasSearchFilter) {
      filteredUsers = filteredUsers.filter((user) =>
        user?.email?.toLowerCase().includes(filterValue.toLowerCase())
      );
    }
    if (
      statusFilter !== "all" &&
      Array.from(statusFilter).length !== statusOptions.length
    ) {
      filteredUsers = filteredUsers.filter((user) =>
        Array.from(statusFilter).includes(user.approve)
      );
    }

    return filteredUsers;
  }, [allUser, filterValue, hasSearchFilter, statusFilter]);
  const pages = Math.ceil(filteredItems.length / rowsPerPage);

  const items = React.useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return filteredItems.slice(start, end);
  }, [page, filteredItems, rowsPerPage]);

  const sortedItems = React.useMemo(() => {
    return [...items].sort((a, b) => {
      const first = a[sortDescriptor.column];
      const second = b[sortDescriptor.column];
      const cmp = first < second ? -1 : first > second ? 1 : 0;

      return sortDescriptor.direction === "descending" ? -cmp : cmp;
    });
  }, [sortDescriptor, items]);

  // Approve user
  const approveUser = (id) => {
    setShowModules(true);
    ApproveUser(id).then((res) => {
      if (res?.data?.message === "successfully approve users") {
        GetAllUser().then((res) => {
          setAllUser(res?.data?.payload);
          console.log(res);
        });
        setShowModules(false);
        console.log(res, "success");
      }
      setShowModules(false);
    });
  };

  const getAllUsers = () => {
    GetAllUser().then((res) => {
      setAllUser(res?.data?.payload);
      console.log(res, "All users");
    });
  };

  const getApproveUsers = () => {
    GetApproveUsers().then((res) => {
      setAllUser(res?.data?.payload);
      console.log(res, "All approve users");
    });
  };

  const getUnApproveUsers = () => {
    GetUnApproveUsers().then((res) => {
      setAllUser(res?.data?.payload);
      console.log(res, "All Un approve users");
    });
  };

  const formattedDate = (inputDate) => {
    const date = new Date(inputDate);
    const options = { day: "numeric", month: "long", year: "numeric" };
    const formattedDate = date
      .toLocaleDateString("en-GB", options)
      .replace(/ /g, "-");
    return formattedDate;
  };

  const [userSelect, setUserSelect] = useState({});

  const getUserById = (id) => {
    GetUserById(id).then((res) => {
      console.log(res, "User by id");

      if (res.status === 200) {
        setUserSelect(res?.data?.payload);
      }
    });
  };

  const [idSelect, setIdSelect] = useState(null);

  console.log(idSelect, "User by idd");

  const [showModuleDel, setModuleDel] = useState(false);
  const [deleteId, setDeleteId] = useState(true);
  const deleteRequestUser = (id) => {
    DeleteReqUser(id)
      .then((res) => {
        console.log(res, "Delete request user");
        getAllUsers();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const renderCell = React.useCallback(
    (user, columnKey) => {
      const cellValue = user[columnKey];

      switch (columnKey) {
        case "info":
          return (
            <div className="flex flex-col">
              <div className="flex justify-between gap-3">
                <div className="flex flex-col">
                  <p className="text-small font-bold">@{user?.username}</p>
                  <p className="text-tiny text-default-500">
                    ID: {user?.chatId}
                  </p>
                </div>
                <Chip
                  className="capitalize text-tiny md:text-small lg:text-small"
                  color={statusColorMap[user.approve]}
                  size="sm"
                  variant="flat"
                >
                  {user.approve ? "Approved" : "Waiting"}
                </Chip>
              </div>

              <div className="flex justify-between mt-2">
                <div className="text-tiny">First name</div>
                <p className="font-semibold text-tiny md:text-small lg:text-small">
                  {user?.firstname}
                </p>
              </div>

              <div className="flex justify-between">
                <div className="text-tiny">Last name</div>
                <p className="font-semibold text-tiny text-default-500">
                  {user?.lastname ? user?.lastname : "N/A"}
                </p>
              </div>

              <div className="flex justify-between">
                <div className="text-tiny">Email</div>
                <p className="font-semibold text-tiny md:text-small lg:text-small">
                  {user?.email}
                </p>
              </div>

              <div className="flex justify-between">
                <div className="text-tiny">Phone number</div>
                <p className="font-semibold text-tiny md:text-small lg:text-small">
                  {user?.phoneNumber}
                </p>
              </div>

              <div className="flex justify-between">
                <div className="text-tiny">Point</div>
                <p className="font-semibold text-tiny text-default-500">
                  {user?.point} pt
                </p>
              </div>

              <div className="flex justify-between">
                <div className="text-tiny">Registered</div>
                <p className="font-semibold text-tiny text-default-500">
                  {formattedDate(user?.createdDate)}
                </p>
              </div>

              <Divider className="mt-4" />
            </div>
          );
        case "username":
          return (
            <div className="flex flex-col">
              <p className="text-bold text-tiny md:text-small lg:text-small capitalize">
                @{user?.username ? user?.username : "N/A"}
              </p>
            </div>
          );
        case "approve":
          return (
            <Chip
              className="capitalize text-tiny md:text-small lg:text-small"
              color={statusColorMap[user.approve]}
              size="sm"
              variant="flat"
            >
              {user.approve ? "Approved" : "Waiting"}
            </Chip>
          );
        case "actions":
          return (
            <div className="relative flex justify-start items-center gap-2">
              <Dropdown>
                <DropdownTrigger>
                  {/* {allUser.map((item, key) => ( */}
                  <Button
                    onClick={() => {
                      getUserById(user?.id);
                      setIdSelect(user?.id);
                    }}
                    isIconOnly
                    size="sm"
                    variant="light"
                  >
                    <VerticalDotsIcon className="text-gray-500" />
                  </Button>
                </DropdownTrigger>
                <DropdownMenu>
                  {/* <Button onPress={onOpen}>Open Modal</Button> */}
                  <DropdownItem onClick={user?.id !== null ? onOpen : false}>
                    View Information
                  </DropdownItem>
                  {!user?.approve && (
                    <DropdownItem
                      onClick={() => {
                        approveUser(user.id);
                      }}
                    >
                      Approve
                    </DropdownItem>
                  )}
                  {!user?.approve && (
                    <DropdownItem
                      onClick={() => {
                        setModuleDel(true), setDeleteId(user?.id);
                      }}
                    >
                      <span className="text-red-700">Delete</span>
                    </DropdownItem>
                  )}
                </DropdownMenu>
              </Dropdown>
            </div>
          );
        default:
          return cellValue;
      }
    },
    [onOpen]
  );

  const onNextPage = React.useCallback(() => {
    if (page < pages) {
      setPage(page + 1);
    }
  }, [page, pages]);

  const onPreviousPage = React.useCallback(() => {
    if (page > 1) {
      setPage(page - 1);
    }
  }, [page]);

  const onRowsPerPageChange = React.useCallback((e) => {
    setRowsPerPage(Number(e.target.value));
    setPage(1);
  }, []);

  const onSearchChange = React.useCallback((value) => {
    if (value) {
      setFilterValue(value);
      setPage(1);
    } else {
      setFilterValue("");
    }
  }, []);

  const onClear = React.useCallback(() => {
    setFilterValue("");
    setPage(1);
  }, []);

  const topContent = React.useMemo(() => {
    return (
      <div className="flex flex-col gap-4">
        <div className="md:flex lg:flex justify-between gap-3 items-end">
          <Input
            isClearable
            className="w-full sm:max-w-[44%]"
            placeholder="Search by email..."
            startContent={<SearchIcon />}
            value={filterValue}
            onClear={() => onClear()}
            onValueChange={onSearchChange}
          />
          <div className="flex max-sm:mt-2 max-md:mt-2 gap-3">
            <Dropdown>
              <DropdownTrigger className=" sm:flex">
                <Button
                  endContent={<ChevronDownIcon className="text-small" />}
                  variant="flat"
                >
                  Filter
                </Button>
              </DropdownTrigger>
              <DropdownMenu
                disallowEmptySelection
                aria-label="Table Columns"
                closeOnSelect={false}
                // selectedKeys={statusFilter}
                selectionMode="single"
              // onSelectionChange={setStatusFilter}
              >
                <DropdownItem onClick={getAllUsers} className="capitalize">
                  All
                </DropdownItem>
                <DropdownItem onClick={getApproveUsers} className="capitalize">
                  Approve
                </DropdownItem>
                <DropdownItem
                  onClick={getUnApproveUsers}
                  className="capitalize"
                >
                  Un Approve
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
            <Dropdown>
              <DropdownTrigger className="sm:flex">
                <Button
                  endContent={<ChevronDownIcon className="text-small" />}
                  variant="flat"
                >
                  Columns
                </Button>
              </DropdownTrigger>
              <DropdownMenu
                disallowEmptySelection
                aria-label="Table Columns"
                closeOnSelect={false}
                selectedKeys={visibleColumns}
                selectionMode="multiple"
                onSelectionChange={setVisibleColumns}
              >
                {columns.map((column) => (
                  <DropdownItem key={column.uid} className="capitalize">
                    {capitalize(column.name)}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown>
            {/* <Button color="primary" endContent={<PlusIcon />}>
              Add New
            </Button> */}
          </div>
        </div>
        <div className="flex justify-between items-center">
          <span className="text-default-400 text-small">
            Total {allUser.length} users
          </span>
          <label className="flex items-center text-default-400 text-small">
            Rows per page:
            <select
              className="bg-transparent outline-none text-default-400 text-small"
              onChange={onRowsPerPageChange}
            >
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="15">15</option>
            </select>
          </label>
        </div>
      </div>
    );
  }, [
    filterValue,
    onSearchChange,
    visibleColumns,
    allUser.length,
    onRowsPerPageChange,
    onClear,
  ]);

  const bottomContent = React.useMemo(() => {
    return (
      <div className="py-2 px-2 flex justify-between items-center">
        <span className="w-[30%] text-small text-default-400">
          {selectedKeys === "all"
            ? "All items selected"
            : `${selectedKeys.size} of ${filteredItems.length} selected`}
        </span>
        <Pagination
          isCompact
          showControls
          showShadow
          color="primary"
          page={page}
          total={pages}
          onChange={setPage}
        />
        <div className="hidden sm:flex w-[30%] justify-end gap-2">
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onPreviousPage}
          >
            Previous
          </Button>
          <Button
            isDisabled={pages === 1}
            size="sm"
            variant="flat"
            onPress={onNextPage}
          >
            Next
          </Button>
        </div>
      </div>
    );
  }, [
    selectedKeys,
    filteredItems.length,
    page,
    pages,
    onPreviousPage,
    onNextPage,
  ]);
  return (
    <div>
      <Modal
        className="absolute z-[999999] max-w-[36rem]"
        isOpen={idSelect !== null ? isOpen : false}
        onOpenChange={onOpenChange}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                View user information
              </ModalHeader>
              <ModalBody className="text-[16px]">
                <div className="flex gap-8 justify-center items-center">
                  <div className="w-96">
                    <table>
                      <tr>
                        <td className="w-28 font-[700]">First name:</td>
                        <td>{userSelect?.firstname}</td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Last name:</td>
                        <td>
                          {userSelect?.lastname ? userSelect?.lastname : "N/A"}
                        </td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Username:</td>
                        <td>
                          {userSelect?.username ? userSelect?.username : "N/A"}
                        </td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Email:</td>
                        <td>{userSelect?.email}</td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Phone:</td>
                        <td>{userSelect?.phoneNumber}</td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Point:</td>
                        <td>
                          {userSelect?.point < 9
                            ? "0" + userSelect?.point
                            : userSelect?.point}{" "}
                          pt
                        </td>
                      </tr>
                      <tr>
                        <td className="w-28 font-[700]">Registered:</td>
                        <td>
                          <p>{formattedDate(userSelect?.createdDate)}</p>
                        </td>
                      </tr>
                      <tr>
                        <td colSpan={2}>
                          <Button
                            disabled
                            color={userSelect?.approve ? "success" : "danger"}
                            variant="flat"
                            className="mt-2"
                          >
                            {userSelect?.approve ? "Approved" : "Not Approved"}
                          </Button>
                        </td>
                      </tr>
                    </table>
                    {/* <p>First name : {userSelect?.firstname}</p>
                    <p>Last name : {userSelect?.lastname}</p>
                    <p>Username : {userSelect?.username}</p>
                    <p>Email : {userSelect?.email}</p>
                    <p>Point : {userSelect?.point}</p>
                    <p>
                      Registered on :{formattedDate(userSelect?.createdDate)}
                    </p> */}
                  </div>

                  <div>
                    <Image
                      src={`${BASE_URL_IMAGE + userSelect?.transaction}`}
                      alt=""
                    />
                  </div>
                </div>
                <Divider />
              </ModalBody>
              <ModalFooter>
                {!userSelect?.approve && (
                  <Button
                    onClick={() => {
                      approveUser(idSelect), setShowModules(false);
                    }}
                    color="primary"
                    onPress={onClose}
                  >
                    Approve
                  </Button>
                )}
                <Button color="danger" variant="flat" onPress={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>

      <Table
        aria-label="Example table with custom cells, pagination and sorting"
        isHeaderSticky
        color={selectedColor}
        selectionMode="single"
        bottomContent={bottomContent}
        bottomContentPlacement="outside"
        classNames={{
          wrapper: "max-h-[382px]",
        }}
        selectedKeys={selectedKeys}
        // selectionMode="multiple"
        sortDescriptor={sortDescriptor}
        topContent={topContent}
        topContentPlacement="outside"
        onSelectionChange={setSelectedKeys}
        onSortChange={setSortDescriptor}
      >
        <TableHeader columns={headerColumns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody emptyContent={"No users found"} items={sortedItems}>
          {(item) => (
            <TableRow key={item.id}>
              {(columnKey) => (
                <TableCell>{renderCell(item, columnKey)}</TableCell>
                // <PhoneView />
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
      {showModules && (
        <div className="flex fixed justify-center items-start z-100 overflow-x-hidden overflow-y-hidden w-full md:inset-0 h-[calc(100%-1rem)] md:h-full backdrop-blur-[2px] bg-black/30">
          <div className="mt-80">
            <LoadingComponentGray />
          </div>
        </div>
      )}
      {showModuleDel && (
        <div className="flex fixed justify-center items-center z-[99999] overflow-x-hidden overflow-y-hidden w-full md:inset-0 h-[calc(100%-1rem)] md:h-full backdrop-blur-[2px] bg-black/30">
          <div className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
            <div>
              <div className="mt-2">
                <h1 className="font-semibold">Are you you to delete?</h1>
                <p className="text-sm mt-1 text-gray-500">
                  click button yes, sure you will delete this user without backup.
                </p>
              </div>
              <div className="flex justify-end items-center mt-4">
                <button
                  type="button"
                  onClick={() => { deleteRequestUser(deleteId), setModuleDel(false) }}
                  className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                >
                  Yes, sure
                </button>
                <button
                  type="button"
                  onClick={() => setModuleDel(false)}
                  className="inline-flex ml-4 justify-center rounded-md border border-transparent bg-red-100 px-4 py-2 text-sm font-medium text-red-900 hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-red-500 focus-visible:ring-offset-2"
                >
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
