import { useEffect } from 'react';
import { useRouter } from 'next/navigation';

const ProtectedRoute = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    // Check if we are on the client-side before accessing localStorage
    if (typeof window !== 'undefined') {
      const token = localStorage.getItem("Token");
      if (!token) {
        router.push("/");
      }
    }
  }, [router]); // Empty dependency array means this useEffect runs once, similar to componentDidMount

  return <>{children}</>;
};

export default ProtectedRoute;